FROM python:3.6-slim
ENV PYTHONUNBUFFERED 1
WORKDIR /app
COPY . /app
RUN pip install -r requirements.txt
EXPOSE 80
ENV FLASK_APP app.py
CMD ["python", "app.py"]