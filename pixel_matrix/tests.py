# Validation and Test Functions
import unittest
from faker import Faker
from . import commands
from .models import Matrix
from .validation import *
from flask import jsonify


class TestMatrix(unittest.TestCase):
    def setUp(self):
        self.fake = Faker()
        self.matrix = Matrix()
        self.matrix.x = self.fake.random_digit()
        self.matrix.y = self.fake.random_digit()
        self.matrix.pixel_map = self.matrix.create_pixels(self.matrix.x, self.matrix.y)

    def test_matrix_creation(self):
        self.assertIsInstance(self.matrix, Matrix)

    def test_matrix_x(self):
        expected_x = self.matrix.x
        self.assertEqual(expected_x, self.matrix.x)


class TestValidations(unittest.TestCase):
    def setUp(self):
        self.fake = Faker()
        self.command = self.fake.random_uppercase_letter()

    def test_command_valid(self):
        valid = ['I', 'C', 'L', 'V', 'H', 'K', 'F']
        expected = self.command in valid
        self.assertEqual(expected, is_command_valid(self.command))

    def test_decimal_values_true(self):
        elements = list(str(val) for val in range(0, 1000))
        random = self.fake.random_sample(elements=list(range(0, 1000)), length=None)
        self.assertEqual(True, are_values_decimal(elements, *random))

    def test_decimal_values_false(self):
        fake_string = self.fake.pystr(min_chars=None, max_chars=20)
        elements = list(fake_string)
        random = self.fake.random_sample(elements=list(range(0, len(elements))), length=None)
        self.assertEqual(False, are_values_decimal(elements, *random))

    def test_command_min_len(self):
        expected = {'I': 3, 'C': 1, 'L': 4, 'V': 5, 'H': 5, 'K': 6, 'F': 4}.get(self.command, -1)
        self.assertEqual(expected, get_command_min_len(self.command))


class KnownResults(unittest.TestCase):
    def setUp(self):
        self.fake = Faker()

    # Test is failing because Flash has no app context
    def test_case_one(self):
        instructions = ["I 5 6", "L 2 3 A", "L 2 3 J", "V 2 3 4 W", "H 3 4 2 Z", "F 3 3 J"]
        expected_json = jsonify({
            "result": [
                "JJJJJ",
                "JJZZJ",
                "JWJJJ",
                "JWJJJ",
                "JJJJJ",
                "JJJJJ"
            ]
        })
        result_matrix, _ = commands.process(instructions, False)
        self.assertEqual(expected_json, commands.matrix_to_json(result_matrix))

    def test_case_two(self):
        instructions = ["I 5 6", "L 2 3 A", "G 2 3 J", "V 2 3 4 W", "H 3 4 2 Z", "F 3 3 J"]
        expected_html = "JJJJJ<br/>JJZZJ<br/>JWJJJ<br/>JWJJJ<br/>JJJJJ<br/>JJJJJ"
        result_matrix, _ = commands.process(instructions, False)
        self.assertEqual(expected_html, commands.matrix_to_string(result_matrix))

    # Test is failing because Flash has no app context
    def test_case_three(self):
        instructions = ["I 10 9", "L 5 3 A", "G 2 3 J", "V 2 3 4 W", "H 1 10 5 Z", "F 3 3 J", "K 2 7 8 8 E", "F 9 9 R"]
        expected_json = {
            "result": [
                "JJJJJJJJJJ",
                "JJJJJJJJJJ",
                "JWJJAJJJJJ",
                "JWJJJJJJJJ",
                "ZZZZZZZZZZ",
                "RRRRRRRRRR",
                "REEEEEEERR",
                "REEEEEEERR",
                "RRRRRRRRRR"
            ]
        }
        result_matrix, _ = commands.process(instructions, False)
        self.assertEqual(expected_json, commands.matrix_to_json(result_matrix))


