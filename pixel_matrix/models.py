# Classes


class Matrix:
    x = 0
    y = 0
    pixel_map = {}

    def is_x_valid(self, x):
        return 0 <= x < self.x

    def is_y_valid(self, y):
        return 0 <= y < self.y

    def create_pixels(self, x, y):
        self.x = x
        self.y = y
        self.clean_pixels()

    def clean_pixels(self):
        self.pixel_map = [['O' for _ in range(self.x)] for _ in range(self.y)]

    def get_pixel_value(self, x, y):
        if self.is_x_valid(x) and self.is_y_valid(y):
            # Access order is inverted because it is created as a list of columns
            return self.pixel_map[y][x]
        else:
            return None

    def set_pixel_value(self, x, y, val):
        if self.is_x_valid(x) and self.is_y_valid(y):
            # Access order is inverted because it is created as a list of columns
            self.pixel_map[y][x] = val
