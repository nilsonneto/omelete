# Validation functions


def is_command_valid(command):
    is_command = {
        'I': True,
        'C': True,
        'L': True,
        'V': True,
        'H': True,
        'K': True,
        'F': True
    }.get(command, False)
    return is_command


def are_values_decimal(array, *indexes):
    return all(array[val].isdecimal() for val in indexes)


def get_command_min_len(command):
    return {
        'I': 3,
        'C': 1,
        'L': 4,
        'V': 5,
        'H': 5,
        'K': 6,
        'F': 4
    }.get(command, -1)


def are_parameters_valid(command_array):
    command = command_array[0]
    command_min_len = get_command_min_len(command)
    if command_min_len == -1 or len(command_array) < command_min_len:
        return False

    # Validates parameter values
    if command == 'I':
        return are_values_decimal(command_array, 1, 2)
    elif command == 'C':
        return True
    elif command == 'L':
        return are_values_decimal(command_array, 1, 2)
    elif command == 'V':
        return are_values_decimal(command_array, 1, 2, 3)
    elif command == 'H':
        return are_values_decimal(command_array, 1, 2, 3)
    elif command == 'K':
        return are_values_decimal(command_array, 1, 2, 3, 4)
    elif command == 'F':
        return are_values_decimal(command_array, 1, 2)

    return False


def is_instruction_valid(command_array):
    command = command_array[0]

    return is_command_valid(command) and are_parameters_valid(command_array)