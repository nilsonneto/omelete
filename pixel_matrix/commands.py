from flask import jsonify
from pixel_matrix.validation import is_instruction_valid
from pixel_matrix.models import Matrix

# Functions to process instructions


def flood_fill_step(queue, matrix, original, replacement):
    node = queue.pop()
    y = node[1]
    # if core node is no longer the same color as original, it was already processed
    if matrix.get_pixel_value(node[0], y) != original:
        return queue

    west = node[0]
    east = node[0]
    while matrix.get_pixel_value(west - 1, y) == original:
        west -= 1
    while matrix.get_pixel_value(east + 1, y) == original:
        east += 1
    for x in range(west, east + 1):
        matrix.set_pixel_value(x, y, replacement)
        north = y - 1
        south = y + 1
        if matrix.get_pixel_value(x, north) == original:
            queue.append((x, north))
        if matrix.get_pixel_value(x, south) == original:
            queue.append((x, south))
    return queue


def flood_fill(matrix, x, y, original, replacement):
    if original == replacement:
        return
    if matrix.get_pixel_value(x, y) != original:
        return
    stack = [(x, y)]
    should_run = True
    while should_run:
        stack = flood_fill_step(stack, matrix, original, replacement)
        if not stack:
            should_run = False


def fill_matrix_at(matrix, x, y, color):
    flood_fill(matrix, x, y, matrix.get_pixel_value(x, y), color)


def process_instruction(command_array, matrix):
    command = command_array[0]
    result = True

    if command == 'I':
        matrix.create_pixels(int(command_array[1]), int(command_array[2]))
    elif command == 'C':
        if matrix.x > 0 and matrix.y > 0:
            matrix.clean_pixels()
        else:
            result = False
    elif command == 'L':
        x = int(command_array[1]) - 1
        y = int(command_array[2]) - 1
        color = command_array[3]
        if matrix.is_x_valid(x) and matrix.is_y_valid(y):
            matrix.set_pixel_value(x, y, color)
        else:
            result = False
    elif command == 'V':
        x = int(command_array[1]) - 1
        y1 = int(command_array[2]) - 1
        y2 = int(command_array[3]) - 1
        color = command_array[4]
        if matrix.is_x_valid(x) and matrix.is_y_valid(y1) and matrix.is_y_valid(y2):
            for y in range(y1, y2 + 1):
                matrix.set_pixel_value(x, y, color)
        else:
            result = False
    elif command == 'H':
        x1 = int(command_array[1]) - 1
        x2 = int(command_array[2]) - 1
        y = int(command_array[3]) - 1
        color = command_array[4]
        if matrix.is_x_valid(x1) and matrix.is_x_valid(x2) and matrix.is_y_valid(y):
            for x in range(x1, x2 + 1):
                matrix.set_pixel_value(x, y, color)
        else:
            result = False
    elif command == 'K':
        x1 = int(command_array[1]) - 1
        x2 = int(command_array[2]) - 1
        y1 = int(command_array[3]) - 1
        y2 = int(command_array[4]) - 1
        color = command_array[5]
        if matrix.is_x_valid(x1) and matrix.is_x_valid(x2) and matrix.is_y_valid(y1) and matrix.is_y_valid(y2):
            for x in range(x1, x2 + 1):
                for y in range(y1, y2 + 1):
                    matrix.set_pixel_value(x, y, color)
        else:
            result = False
    elif command == 'F':
        x = int(command_array[1]) - 1
        y = int(command_array[2]) - 1
        color = command_array[3]
        if matrix.is_x_valid(x) and matrix.is_y_valid(y):
            fill_matrix_at(matrix, x, y, color)
        else:
            result = False

    return result, matrix


def process(instruction_list, fail_at_invalid):
    matrix = Matrix()
    error = None
    if instruction_list:
        for instruction in instruction_list:
            command_array = instruction.split(" ")
            if not is_instruction_valid(command_array):
                error = 'Invalid instruction ' + instruction + '; '
                if fail_at_invalid:
                    break
            else:
                result, matrix = process_instruction(command_array, matrix)
                if not result:
                    error = 'Instruction with error ' + instruction + '; '
                    if fail_at_invalid:
                        break
    else:
        error = "Invalid instruction list"

    return matrix, error


# Functions to print pixel matrix


def print_matrix(matrix):
    for lines in matrix.pixel_map:
        print(" ".join(lines))


def matrix_to_string(matrix):
    return "<br/>".join("".join(lines) for lines in matrix.pixel_map)


def matrix_to_json(matrix):
    return jsonify({"result": ["".join(lines) for lines in matrix.pixel_map]})
