#!/usr/bin/env python

from pixel_matrix.commands import process, matrix_to_string, matrix_to_json
from flask import Flask, request, jsonify, render_template
from jinja2 import Environment, PackageLoader, select_autoescape

app = Flask(__name__)
env = Environment(
    loader=PackageLoader('pixel_matrix', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)
fail_on_wrong_instruction = False


@app.route("/")
def hello():
    template = env.get_template('home.html')
    return render_template(template)


@app.route("/matrix/", methods=['GET', 'POST'])
def pixel_matrix():
    instruction_list = request.data.decode("utf-8").split('\n')
    matrix, error = process(instruction_list, fail_on_wrong_instruction)
    page = matrix_to_string(matrix)
    if fail_on_wrong_instruction:
        return error if error else page
    else:
        return page


@app.route("/matrix.json/", methods=['GET', 'POST'])
def json_pixel_matrix():
    instruction_list = request.get_json()
    matrix, error = process(instruction_list, fail_on_wrong_instruction)
    page = matrix_to_json(matrix)
    if fail_on_wrong_instruction:
        return jsonify({"error": error}) if error else page
    else:
        return page


@app.errorhandler(404)
def page_not_found(error):
    template = env.get_template('not_found.html')
    return render_template(template), 404


@app.errorhandler(405)
def page_not_found(error):
    template = env.get_template('not_allowed.html')
    return render_template(template), 405


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)
