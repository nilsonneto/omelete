# Omelete Matrix Project

Foi escolhido utilizar Flask, pois para utilizar Django-REST-Framework seria necessário implementar quebrar alguns padrões do framework.

Utilizou-se também Faker para facilitar a geração de dados aleatórios. 

## Rodando o servidor
### Localmente
Para rodar localmente, basta executar ```python -m flask run``` em um prompt que tenha o ambiente virtual rodando.
### Dockerfile
Apos efetuar o build da imagem, rodá-la, expondo sua porta 80 para a porta desejada.
Ex.: `docker run -p 4000:80 pixel`, onde 4000 é a porta externa e 80 é a porta da imagem.
